//
//  Route.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import Foundation
import UIKit

extension PokemonListViewController {
    func routeDetail(pokemon: Pokemon) {
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: kIdentifierPokemonDetailViewController) as? PokemonDetailViewController {
            vc.pokemon = pokemon
            self.navigationController?.pushViewController(vc, animated: false)
        }
    }
}
