//
//  LocalService.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import Foundation
import RealmSwift

class LocalDataService {
    static let realm = try! Realm()
    
    static func saveData<T>(_ data: T, success:@escaping (_ success: Bool) -> Void) {
        do {
            print(Realm.Configuration.defaultConfiguration.fileURL ?? "")
            try realm.write {
                if let galery = data as? [Galery] {
                    realm.add(galery, update: true)
                    success(true)
                }

                if let pokemon = data as? Pokemon {
                    realm.add(pokemon, update: true)
                    success(true)
                }
            }
        } catch _ as NSError {
            success(false)
        }
    }
    
    static func fetchData<T: Object>(_ type: T.Type, _ id: String = "", format: String = "") -> Results<T> {
        return id != "" ? realm.objects(type).filter(NSPredicate(format: format, id)) : realm.objects(type)
    }
    
    static func removeLocalData(){
        try! realm.write {
            realm.deleteAll()
        }
    }
}
