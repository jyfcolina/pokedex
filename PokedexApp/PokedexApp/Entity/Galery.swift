//
//  Galery.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Galery: Object {
    dynamic var id : String = ""
    dynamic var name : String = ""
    dynamic var image : String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

extension Galery: Mappable {
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        image <- map["img"]
    }
}
