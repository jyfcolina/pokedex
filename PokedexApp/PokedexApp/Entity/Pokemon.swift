//
//  Pokemon.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Pokemon: Object {
    dynamic var number : Int = 0
    dynamic var name : String = ""
    dynamic var type : String = ""
    dynamic var descriptions : String = ""
    dynamic var image : String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    override static func primaryKey() -> String? {
        return "number"
    }
}

extension Pokemon: Mappable {
    func mapping(map: Map) {
        number <- map["number"]
        name <- map["name"]
        type <- map["type"]
        descriptions <- map["descriptions"]
        image <- map["image"]
    }
}
