//
//  PokemonCollectionViewCell.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import UIKit
import Kingfisher

let kIdentifierPokemonCollectionViewCell = "PokemonCollectionViewCell"

class PokemonCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var pokeImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func pokemon(pokemon: Pokemon) {
        if let url = URL(string: pokemon.image) {
            self.pokeImageView.kf.setImage(with: url)
            self.nameLabel.text = pokemon.name
        }
    }
    
}
