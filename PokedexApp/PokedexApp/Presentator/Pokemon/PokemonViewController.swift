//
//  PokemonViewController.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import UIKit
import Kingfisher

class PokemonViewController: UIViewController {

    @IBOutlet weak var pokeImageView: UIImageView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextView!
    @IBOutlet weak var rigthButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    
    let interactor = Interactor()
    var galery: [Galery]?
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor.delegate = self
        self.descriptionTextField.delegate = self
        interactor.retrieveData()
        self.navigationItem.title = "New Pokémon"
        self.descriptionTextField.text = "Description"
        self.descriptionTextField.textColor = UIColor.gray
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(addTappedRigth))
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(addTappedLeft))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        if let galery = self.galery, self.index < galery.count - 1 {
            self.index += 1
            if let url = URL(string: galery[index].image) {
                self.pokeImageView.kf.setImage(with: url)
            }
        }
    }

    @IBAction func previousButtonTapped(_ sender: UIButton) {
        if let galery = self.galery, self.index > 0 {
            self.index -= 1
            if let url = URL(string: galery[index].image) {
                self.pokeImageView.kf.setImage(with: url)
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PokemonViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == "" {
            self.descriptionTextField.text = "Description"
            self.descriptionTextField.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Description" {
            textView.textColor = UIColor.black
            self.descriptionTextField.text = ""
        }
    }
}

extension PokemonViewController: InteractorDelegate {
    func reloadList(fotoGallery galery: [Galery]) {
        self.galery = galery
        if let url = galery.first?.image {
            self.pokeImageView.kf.setImage(with: URL(string: url))
        }
    }
    
    func successEvent(success: Bool) {
        if success {
            addTappedLeft()
            UIViewController.successEvent(message: "Pokémon saved success full")
        } else {
            UIViewController.successEvent(message: "Error")
        }
    }
}

extension PokemonViewController {
    func addTappedRigth() {
        if let number = self.numberTextField.text, number != "", let name = self.nameTextField.text, name != "", let type = self.typeTextField.text, type != "", let description = self.descriptionTextField.text, description != "Description" {
            self.interactor.savePokemon(image: self.galery![index].image, number: Int(number)!, name: name, type: type, description: description)
        }
    }
    
    func addTappedLeft() {
        self.numberTextField.text = ""
        self.nameTextField.text = ""
        self.typeTextField.text = ""
        self.descriptionTextField.text = "Description"
        self.descriptionTextField.textColor = UIColor.lightGray
    }
}
