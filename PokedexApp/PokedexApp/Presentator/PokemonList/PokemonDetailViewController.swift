//
//  PokemonDetailViewController.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import UIKit

let kIdentifierPokemonDetailViewController = "PokemonDetailViewController"

class PokemonDetailViewController: UIViewController {

    @IBOutlet weak var pokeImageView: UIImageView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var pokemon: Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "\(self.pokemon!.name)"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.pokemonDetail()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.descriptionTextView.scrollRangeToVisible(NSMakeRange(0, 0))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pokemonDetail() {
        if let url = URL(string: pokemon!.image) {
            self.pokeImageView.kf.setImage(with: url)
            self.nameLabel.text = self.pokemon!.name
            self.typeLabel.text = self.pokemon!.type
            self.numberLabel.text = "\(self.pokemon!.number)"
            self.descriptionTextView.text = self.pokemon!.descriptions
        }
    }

}
