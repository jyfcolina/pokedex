//
//  PokemonListViewController.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import UIKit

class PokemonListViewController: UIViewController {

    @IBOutlet weak var pokemonCollectionView: UICollectionView!
    
    var pokemonList: [Pokemon]?
    
    let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    let interactor = Interactor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.interactor.delegate = self
        self.navigationItem.title = "Pokémon List"
        self.pokemonCollectionView.register(UINib(nibName: kIdentifierPokemonCollectionViewCell, bundle: nil), forCellWithReuseIdentifier: kIdentifierPokemonCollectionViewCell)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.interactor.retrieveList()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   

}

// MARK: UICollectionViewDataSource
extension PokemonListViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.pokemonList != nil ? 1 : 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pokemonList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: kIdentifierPokemonCollectionViewCell, for: indexPath) as! PokemonCollectionViewCell
        cell.pokemon(pokemon: self.pokemonList![indexPath.row])
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension PokemonListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.routeDetail(pokemon: self.pokemonList![indexPath.row])
    }
}

// MARK: UICollectionViewDelegateFlowLayout
extension PokemonListViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 120, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

// MARK: InteractorDelegate

extension PokemonListViewController: InteractorDelegate {
    func reloadList(listPokemon pokemons: [Pokemon]) {
        self.pokemonList = pokemons
        self.pokemonCollectionView.reloadData()
    }
}
