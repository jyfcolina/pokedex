//
//  Interactor.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: InteractorDelegate - Protocol
@objc protocol InteractorDelegate: class {
    @objc optional func successEvent(success : Bool)
    @objc optional func reloadList(fotoGallery galery: [Galery])
    @objc optional func reloadList(listPokemon pokemons: [Pokemon])
}

class Interactor {
    
    var delegate: InteractorDelegate?
    let dataManager = DataManager()
    
    func retrieveData() {
        self.dataManager.delegate = self
        self.dataManager.fetchGalery()
    }
    
    func retrieveList() {
        self.dataManager.delegate = self
        self.dataManager.fetchListPokemon()
    }
    
    func savePokemon(image: String, number: Int, name: String, type: String, description: String) {
        self.dataManager.delegate = self
        self.dataManager.saveLocalPokemon(newPokemon: Pokemon(JSON: [
                "image": image,
                "number": number,
                "name": name,
                "type": type,
                "descriptions": description
            ])!)
    }
    
}

// MARK: Interactor - DataManagerDelegate.
extension Interactor: DataManagerDelegate {
    func responseDataManager<T>(_ response: T) {
        if let galery = response as? [Galery], galery.count > 0 {
            self.delegate?.reloadList!(fotoGallery: galery)
        }
        
        if let list = response as? [Pokemon] {
            self.delegate?.reloadList!(listPokemon: list)
        }
        
        if let success = response as? Bool {
            self.delegate?.successEvent!(success: success)
        }
    }
}


// MARK: Extensions
extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        return array
    }
}
