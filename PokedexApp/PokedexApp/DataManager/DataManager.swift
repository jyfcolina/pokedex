//
//  DataManager.swift
//  PokedexApp
//
//  Created by Jyferson Colina on 2/1/17.
//  Copyright © 2017 Jyferson Colina. All rights reserved.
//

import Foundation
import UIKit

protocol DataManagerDelegate: class {
    func responseDataManager <T>(_ response: T)
}

class DataManager {
    
    var delegate : DataManagerDelegate?
    
    // MARK: Galery
    func fetchGalery() {
        if NetworkDataServices.isConnectedToNetwork() {
            self.fetchNetworkGalery()
        } else {
            UIViewController.connectionOutside()
            self.fetchLocalGalery()
        }
    }
    
    func fetchLocalGalery() {
        let galery = LocalDataService.fetchData(Galery.self)
        self.delegate?.responseDataManager(galery.toArray(ofType: Galery.self))
    }
    
    
    
    func fetchNetworkGalery() {
        NetworkDataServices.fetchArrayData(url: "http://capptains.com/etc/pokemon.json", type: Galery.self,success: {(result) in
            self.updateLocalData(listImage: result)
        }, fail: { (error) in
            self.fetchLocalGalery()
        })
    }
    
    func updateLocalData(listImage images: [Galery]) {
        LocalDataService.saveData(images, success: {(save) in
            if save {
                self.fetchLocalGalery()
            }
        })
    }
    
    
    // MARK: Pokemon 
    
    func fetchListPokemon() {
        self.fetchLocalPokemons()
    }
    
    func fetchLocalPokemons() {
        let list = LocalDataService.fetchData(Pokemon.self)
        self.delegate?.responseDataManager(list.toArray(ofType: Pokemon.self))
    }
    
    func saveLocalPokemon(newPokemon pokemon: Pokemon) {
        LocalDataService.saveData(pokemon, success: {(save) in
            if save {
                self.delegate?.responseDataManager(true)
            } else {
                self.delegate?.responseDataManager(false)
            }
        })
    }
    
}
